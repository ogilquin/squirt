# installation matérielle

- vérifier que les pompes soient au fond (ventousées)
- connecter l'electro-vanne (peu importe bleu ou rouge sur la pompe)
- connecter le capteur (fils jaunes) aux wago (peu importe le sens bleu ou vert)
- brancher l'alimentation 12V
- connecter les pompes


# fonctionnement du hardware

- lorsque la pompe est ouverte, il y a un timeout de sécurité de 5s après quoi la pompe se referme. Il est possible de le modifier dans le code du firmware
- s'il n'y a pas suffisament d'eau, la pompe ne s'ouvre pas (un message est envoyé) et un message `tank_empty` est envoyé toutes les 10s