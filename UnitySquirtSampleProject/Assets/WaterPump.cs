﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.IO.Ports; // If can't import package; Goto Edit>Project settings>Player and in Other settings, under Configuration, change the API compatibility level to .NET 2.0 instead of .NET 2.0 subset. This will allow unity to access serial.io class
using UnityEngine;

public class WaterPump : MonoBehaviour {
	public string COM_PORT = "\\\\.\\COM16"; // "COM3" or "\\.\COM16" if port > COM9
	private int BAUD_RATE = 115200;

	public SerialPort serialPort;
	public Thread serialThread;

	public bool open = false;

	private bool lastOpenState = false;

	void Start () {
		serialPort = new SerialPort(COM_PORT, BAUD_RATE);

		try {
			serialPort.Open();
			serialPort.ReadTimeout = 500;
			serialPort.WriteTimeout = 500;
			serialPort.Handshake = Handshake.None;
			serialThread = new Thread(checkSerialInput);
			serialThread.Start();
			Debug.Log("Port Opened");
		} catch (SystemException e) {
			Debug.Log ("Error opening = " + e.Message);
		}
	}
	
	void Update () {
		if (open != lastOpenState) {
			if (open) {
				serialPort.Write("o");
			} else {
				serialPort.Write("c");
			}

			lastOpenState = open;
		}
	}

	void checkSerialInput() {
		if ((serialPort != null) && (serialPort.IsOpen)) {
			while (true) {
				try {
					string message = serialPort.ReadLine();
					if (message.Contains("_closed") || message.Contains("_not_opened") ) {
						handlePumpClosed();
					} else if (message.Contains("_opened")) {
						handlePumpOpened();
					} else if (message.Contains("_empty")) {
						handleTankEmpty();
					}
					Debug.Log("Message received: " + message);
				}
				catch (TimeoutException) {}
			}
		}
	}

	void handlePumpOpened() {
		open = true;
		lastOpenState = open;
		Debug.Log("[PUMP] opened");
	}

	void handlePumpClosed() {
		open = false;
		lastOpenState = open;
		Debug.Log("[PUMP] closed");
	}

	void handleTankEmpty() {
		Debug.Log("[WARNING] tank is empty");
	}
		
}
