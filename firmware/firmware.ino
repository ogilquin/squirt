/*
** led 13 status: clignote=reservoir vide
*                 éteint=pompe fermée
*                 allumée=pompe ouverte
*/

const int PUMP_RELAY_PIN = 2;
const int VALVE_RELAY_PIN = 3;
const int LED_PIN = 13;
const int WATER_SENSOR_PIN = 4;

const int PUMP_OPEN_TIMEOUT = 5000;
const int TANK_CHECK_INTERVAL = 10000;

long pumpOpenTime;
bool pumpOpenState;
bool emptyTank;
long lastCheckTankTime;

void setup() {
  // setup communication
  Serial.begin(115200);
  
  // setup actuators
  pinMode(PUMP_RELAY_PIN, OUTPUT);
  pinMode(VALVE_RELAY_PIN, OUTPUT);
  digitalWrite(PUMP_RELAY_PIN, HIGH);
  digitalWrite(VALVE_RELAY_PIN, HIGH);

  // setup sensor
  pinMode(WATER_SENSOR_PIN, INPUT_PULLUP);

  // setup led status
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  while (Serial.available()) {  
    char letter = Serial.read();
    
    if (letter == 'o') {
      openPump();
    } else if (letter == 'c') {
      closePump();
    }
  }

  checkPump();
  checkTank();
  showLedStatus();
}

void openPump() {
  if (! emptyTank) {
    digitalWrite(PUMP_RELAY_PIN, LOW);
    digitalWrite(VALVE_RELAY_PIN, LOW);
    pumpOpenState = true;
    pumpOpenTime = millis();
    Serial.println("pump_opened");
  } else {
    Serial.println("pump_not_opened");
    Serial.println("tank_empty");
    lastCheckTankTime = millis();
  }
}

void closePump() {
  digitalWrite(PUMP_RELAY_PIN, HIGH);
  digitalWrite(VALVE_RELAY_PIN, HIGH);
  pumpOpenState = false;
  Serial.println("pump_closed");
}

void checkPump() {
  if (pumpOpenState && millis() - pumpOpenTime > PUMP_OPEN_TIMEOUT) {
    Serial.println("pump_timeout");
    closePump();
  } else if (pumpOpenState && emptyTank) {
    Serial.println("tank_empty");
    closePump();
    lastCheckTankTime = millis();
  }
}

void checkTank() {
  emptyTank = digitalRead(WATER_SENSOR_PIN);
  
  if (millis() - lastCheckTankTime > TANK_CHECK_INTERVAL) {
    if (emptyTank) {
      Serial.println("tank_empty");
    }

    lastCheckTankTime = millis();
  }
}

void showLedStatus() {
  if (emptyTank) {
    digitalWrite(LED_PIN, (millis() % 500) > 250 ? HIGH : LOW);
  } else {
    digitalWrite(LED_PIN, pumpOpenState ? HIGH : LOW);
  }
}

